const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: [
    "org/**/*.org",
    "public/**/*.html"
  ],
  darkMode: "class",
  theme: {
    gridTemplateAreas: {
      mobile: ['header header header', 'main main main', 'footer footer footer'],
      desktop: ['sidebar header header', 'sidebar main main', 'sidebar footer footer'],
    }, 
    extend: {
      gridTemplateColumns: {
        layout: '0.7fr 2.3fr 1fr',
      },
      gridTemplateRows: {
        layout: '0.2fr 2.6fr 0.2fr',
      },
      height: {
        '13': '52px',
        '18': '66px'
      },
      screens: {
        mobile: { max: '1023px'}
      },
      fontFamily: {
        alegreya: ["Alegreya", defaultTheme.fontFamily.serif],
        kingHarold: ["'King Harold'", defaultTheme.fontFamily.serif],
      },
      colors: {
        manuwhite: "#f0e3d1",
        manutan: "#d0c4a9",
        heraldic: {
          red: "#bc2e2e",
          blue: {
            base: "#0d6793",
            bright: "#1188c2"
          },
        },
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            fontFamily: [theme("fontFamily.alegreya")],

            "h1,h2,h3,h4,h5,h6": {
              fontFamily: [theme("fontFamily.kingHarold")],
            },
          },
        },

        stone: {
          css: {
            "--tw-prose-bullets": theme("colors.stone[400]"),
            // ...
          },
        },
      }),
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("@savvywombat/tailwindcss-grid-areas"),
    require("tailwind-scrollbar")
  ],
}
