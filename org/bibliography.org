# -*- coding: utf-8; -*-
#+title: Bibliography
#+BIBLIOGRAPHY: ./biblio/references.bib
#+cite_export: csl chicago-note.csl

Here is the bibliography for the project, in case you were interested in what I used for research.

[cite:@BetancourtHiddenQueer2020]

[cite:@BrayFriend2003]

[cite:@BoswellSameSexUnions1994]

[cite:@BoswellChristianitySocial1981]

[cite:@MaaloufCrusadesArab1989]
[cite:@ClarkMedievalMen2009]


[cite:@MillsSeeingSodomy2015]

#+print_bibliography:

