# Created 2022-03-02 Wed 21:22
:PROPERTIES:
:ID:       85db3992-94d5-47ce-827d-f43985ec27c1
:NAMESPACE: Conworlds
:CATEGORY: Broken Thrones
:END:
#+title: Kilshire
#+author: Jacob Hilker
#+filetags: :The__Ashes__Chronicles:BrokenThrones:place:

Kilshire is a barony in central England.
